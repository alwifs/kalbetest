﻿using kalbetest.datamodels;
using kalbetest.viewmodels;
using KalbeTest.Web.Services;
using Microsoft.AspNetCore.Mvc;

namespace KalbeTest.Web.Controllers
{
	public class CostumerController : Controller
	{
		private CostumerService costumerService;
		public CostumerController(CostumerService _costumerService)
		{
			this.costumerService = _costumerService;

		}
		public async Task<IActionResult> Index(string sortOrder,
												string searchString,
												string currentFilter,
												int? pageNumber,
												int? pageSize)
		{
			ViewBag.CurrentSort = sortOrder;
			ViewBag.CurrentPageSize = pageSize;
			ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

			if (searchString != null)
			{
				pageNumber = 1;
			}
			else
			{
				searchString = currentFilter;
			}

			ViewBag.CurrentFilter = searchString;

			List<TblCostumer> data = await costumerService.GetAllData();

			if (!string.IsNullOrEmpty(searchString))
			{
				data = data.Where(a => a.CostumerName.ToLower().Contains(searchString.ToLower())
				|| a.CostumerAddress.ToLower().Contains(searchString.ToLower())
				).ToList();
			}
			switch (sortOrder)
			{
				case "name_desc":
					data = data.OrderByDescending(a => a.CostumerName).ToList();
					break;
				default:
					data = data.OrderBy(a => a.CostumerName).ToList();
					break;
			}
			return View(PaginationList<TblCostumer>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 3));
		}

		public IActionResult Create()
		{
			TblCostumer data = new TblCostumer();
			return PartialView(data);
		}
		[HttpPost]
		public async Task<IActionResult> Create(TblCostumer dataParam)
		{

			VMResponse respon = await costumerService.Create(dataParam);

			if (respon.Success)
			{
				return Json(new { dataRespon = respon });
			}
			return View(dataParam);
		}

		public async Task<IActionResult> Edit(int id)
		{
			TblCostumer data = await costumerService.GetDataById(id);
			return PartialView(data);
		}
		[HttpPost]
		public async Task<IActionResult> Edit(TblCostumer dataParam)
		{

			VMResponse respon = await costumerService.Edit(dataParam);

			if (respon.Success)
			{
				return Json(new { dataRespon = respon });
			}
			return View(dataParam);
		}
		
        public async Task<IActionResult> Delete(int id)
        {
            TblCostumer data = await costumerService.GetDataById(id);
            return PartialView(data);
        }
        [HttpPost]
        public async Task<IActionResult> SureDelete(int CostumerId)
        {
            
            VMResponse respon = await costumerService.Delete(CostumerId);

            if (respon.Success)
            {
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index", CostumerId);
        }
		public async Task<IActionResult> Detail(int id)
		{
			TblCostumer data = await costumerService.GetDataById(id);
			return PartialView(data);
		}

	}
}
