﻿using kalbetest.viewmodels;
using kalbetest.datamodels;
using KalbeTest.Web.Services;
using Microsoft.AspNetCore.Mvc;

namespace KalbeTest.Web.Controllers
{
	public class PenjualanController : Controller
	{
		private ProdukService produkService;
		private CostumerService costumerService;
		private PenjualanService penjualanService;

		public PenjualanController(ProdukService _produkService, CostumerService _costumerService, PenjualanService _penjualanService)
		{
			this.produkService = _produkService;
			this.costumerService = _costumerService;
			this.penjualanService = _penjualanService;
		}

		public async Task<IActionResult> Index(string sortOrder, string searchString, string currentFilter, int? pageNumber, int? pageSize)
		{
			ViewBag.CurrentSort = sortOrder;
			ViewBag.CurrentPageSize = pageSize;
			ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

			if (searchString != null)
			{
				pageNumber = 1;
			}
			else
			{
				searchString = currentFilter;
			}

			ViewBag.CurrentFilter = searchString;

			List<VMTblPenjualan> data = await penjualanService.GetAllData();

			if (!string.IsNullOrEmpty(searchString))
			{
				data = data.Where(a => a.CostumerName.ToLower().Contains(searchString.ToLower())
				|| a.ProductName.ToLower().Contains(searchString.ToLower())
			 ).ToList();
			}

			switch (sortOrder)
			{
				case "name_desc":
					data = data.OrderByDescending(a => a.CostumerName).ToList();
					break;
				default:
					data = data.OrderBy(a => a.CostumerName).ToList();
					break;
			}

			return View(PaginationList<VMTblPenjualan>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 3));
		}

		public async Task<IActionResult> Create()
		{
			VMTblPenjualan data = new VMTblPenjualan();

			List<TblProduk> listProduk = await produkService.GetAllData();
			ViewBag.ListProduk = listProduk;

			List<TblCostumer> listCostumer = await costumerService.GetAllData();
			ViewBag.ListCostumer = listCostumer;

			return PartialView(data);
		}

		[HttpPost]
		public async Task<IActionResult> Create(VMTblPenjualan dataParam)
		{
			VMResponse respon = await penjualanService.Create(dataParam);

			if (respon.Success)
			{	
				return Json(new { dataRespon = respon });
			}
			return View(dataParam);
		}

		public async Task<IActionResult> Edit(int id)
		{
            VMTblPenjualan data = await penjualanService.GetDataById(id);

            List<TblProduk> listProduk = await produkService.GetAllData();
            ViewBag.ListProduk = listProduk;

            List<TblCostumer> listCostumer = await costumerService.GetAllData();
            ViewBag.ListCostumer = listCostumer;

            return PartialView(data);
        }

		[HttpPost]
		public async Task<IActionResult> Edit(VMTblPenjualan dataParam)
		{
			VMResponse respon = await penjualanService.Edit(dataParam);

			if (respon.Success)
			{
				return Json(new { dataRespon = respon });
			}

			return View(dataParam);
		}

		public async Task<IActionResult> Delete(int id)
		{
			VMTblPenjualan data = await penjualanService.GetDataById(id);
			return PartialView(data);
		}

		[HttpPost]
		public async Task<IActionResult> SureDelete(int SalesOrderId)
		{
			VMResponse respon = await penjualanService.Delete(SalesOrderId);

			if (respon.Success)
			{
				return RedirectToAction("Index");
			}
			return RedirectToAction("Index", SalesOrderId);
		}

		public async Task<IActionResult> Detail(int id)
		{
			VMTblPenjualan data = await penjualanService.GetDataById(id);

            List<TblProduk> listProduk = await produkService.GetAllData();
            ViewBag.ListProduk = listProduk;

            List<TblCostumer> listCostumer = await costumerService.GetAllData();
            ViewBag.ListCostumer = listCostumer;

            return PartialView(data);
		}
	}
}
