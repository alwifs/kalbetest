﻿using kalbetest.datamodels;
using kalbetest.viewmodels;
using Newtonsoft.Json;
using System.Text;

namespace KalbeTest.Web.Services
{
	public class CostumerService
	{
		private static readonly HttpClient client = new HttpClient();
		private IConfiguration configuration;
		private string RouteAPI = "";
		private VMResponse respon = new VMResponse();

		public CostumerService(IConfiguration _configuration)
		{
			this.configuration = _configuration;
			this.RouteAPI = this.configuration["RouteAPI"];
		}

		public async Task<List<TblCostumer>> GetAllData()
		{
			List<TblCostumer> data = new List<TblCostumer>();
			string apiResponse = await client.GetStringAsync(RouteAPI + "apiCostumer/GetAllData");

			data = JsonConvert.DeserializeObject<List<TblCostumer>>(apiResponse)!;

			return data;
		}
		public async Task<VMResponse> Create(TblCostumer dataParam)
		{
			// proses convert dari object ke string
			string json = JsonConvert.SerializeObject(dataParam);
			/// proses mengubah string menjadi json lalu dikirim sebagai request body
			StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
			// proses memanggil api dan pengiriman body
			var request = await client.PostAsync(RouteAPI + "apiCostumer/Save", content);


			if (request.IsSuccessStatusCode)
			{
				// proses membaca respon api
				var apiRespon = await request.Content.ReadAsStringAsync();

				// proses 
				respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;

			}
			else
			{
				respon.Success = false;
				respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
			}
			return respon;
		}

		public async Task<TblCostumer> GetDataById(int id)
		{
			TblCostumer data = new TblCostumer();
			string apiRespon = await client.GetStringAsync(RouteAPI + $"apiCostumer/GetDataById/{id}");
			data = JsonConvert.DeserializeObject<TblCostumer>(apiRespon)!;

			return data;
		}

		public async Task<VMResponse> Edit(TblCostumer dataParam)
		{
			// proses convert dari object ke string
			string json = JsonConvert.SerializeObject(dataParam);
			/// proses mengubah string menjadi json lalu dikirim sebagai request body
			StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
			// proses memanggil api dan pengiriman body
			var request = await client.PutAsync(RouteAPI + "apiCostumer/Edit", content);


			if (request.IsSuccessStatusCode)
			{
				// proses membaca respon api
				var apiRespon = await request.Content.ReadAsStringAsync();

				// proses 
				respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;

			}
			else
			{
				respon.Success = false;
				respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
			}
			return respon;
		}
        public async Task<VMResponse> Delete(int id)
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiCostumer/Delete/{id}");

            if (request.IsSuccessStatusCode)
            {
                // proses membaca respon api
                var apiRespon = await request.Content.ReadAsStringAsync();

                // proses 
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;

            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }
        public async Task<VMResponse> Detail(TblCostumer dataParam)
		{
			// proses convert dari object ke string
			string json = JsonConvert.SerializeObject(dataParam);
			/// proses mengubah string menjadi json lalu dikirim sebagai request body
			StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
			// proses memanggil api dan pengiriman body
			var request = await client.PutAsync(RouteAPI + "apiCostumer/Detail", content);


			if (request.IsSuccessStatusCode)
			{
				// proses membaca respon api
				var apiRespon = await request.Content.ReadAsStringAsync();

				// proses 
				respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;

			}
			else
			{
				respon.Success = false;
				respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
			}
			return respon;
		}
	}
}
