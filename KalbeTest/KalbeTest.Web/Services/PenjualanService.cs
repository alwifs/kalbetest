﻿using kalbetest.viewmodels;
using Newtonsoft.Json;
using System.Text;

namespace KalbeTest.Web.Services
{
	public class PenjualanService
	{
		private static readonly HttpClient client = new HttpClient();
		private IConfiguration configuration;
		private string RouteAPI = "";
		private VMResponse respon = new VMResponse();

		public PenjualanService(IConfiguration _configuration)
		{
			this.configuration = _configuration;
			this.RouteAPI = this.configuration["RouteAPI"];
		}

		public async Task<List<VMTblPenjualan>> GetAllData()
		{
			List<VMTblPenjualan> data = new List<VMTblPenjualan>();

			string apiResponse = await client.GetStringAsync(RouteAPI + "apiPenjualan/GetAllData");
			data = JsonConvert.DeserializeObject<List<VMTblPenjualan>>(apiResponse)!;

			return data;
		}

		public async Task<VMTblPenjualan> GetDataById(int id)
		{
			VMTblPenjualan data = new VMTblPenjualan();
			string apiResponse = await client.GetStringAsync(RouteAPI + $"apiPenjualan/GetDataById/{id}");

			data = JsonConvert.DeserializeObject<VMTblPenjualan>(apiResponse)!;
			return data;
		}

		public async Task<VMResponse> Create(VMTblPenjualan dataParam)
		{
			//Proses convert dari object ke string
			string json = JsonConvert.SerializeObject(dataParam);

			//Proses mengubah string menjadi Json lalu di kirim sebagai request body
			StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

			//Proses memanggil API dan mengirimkan body
			var request = await client.PostAsync(RouteAPI + "apiPenjualan/Save", content);

			if (request.IsSuccessStatusCode)
			{
				//Proses membaca respon dari API
				var apiRespon = await request.Content.ReadAsStringAsync();

				//Proses convert hasil respon dari API ke Object
				respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
			}
			else
			{
				respon.Success = false;
				respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
			}
			return respon;
		}

		public async Task<VMResponse> Edit(VMTblPenjualan dataParam)
		{
			//Proses convert dari object ke string
			string json = JsonConvert.SerializeObject(dataParam);

			//Proses mengubah string menjadi Json lalu di kirim sebagai request body
			StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

			//Proses memanggil API dan mengirimkan body
			var request = await client.PutAsync(RouteAPI + "apiPenjualan/Edit", content);

			if (request.IsSuccessStatusCode)
			{
				//Proses membaca respon dari API
				var apiRespon = await request.Content.ReadAsStringAsync();

				//Proses convert hasil respon dari API ke Object
				respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
			}
			else
			{
				respon.Success = false;
				respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
			}
			return respon;
		}

		public async Task<VMResponse> Delete(int id)
		{
			var request = await client.DeleteAsync(RouteAPI + $"apiPenjualan/Delete/{id}");

			if (request.IsSuccessStatusCode)
			{
				//Proses membaca respon dari API
				var apiRespon = await request.Content.ReadAsStringAsync();

				//Proses convert hasil respon dari API ke Object
				respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
			}
			else
			{
				respon.Success = false;
				respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
			}
			return respon;
		}
	}
}
