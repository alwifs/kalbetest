﻿using kalbetest.datamodels;
using kalbetest.viewmodels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace kalbetest.api.Controllers
{
	[Route("[controller]")]
	[ApiController]
	public class apiCostumerController : ControllerBase
	{
		private readonly kalbe_dbContext db;
		private VMResponse respon = new VMResponse();


		public apiCostumerController(kalbe_dbContext _db)
		{
			this.db = _db;
		}

		[HttpGet("GetAllData")]
		public List<TblCostumer> GetAllData()
		{
			List<TblCostumer> data = db.TblCostumers.Where(a => a.IsDelete == false).ToList();
			return data;

		}
		[HttpGet("GetDataById/{id}")]
		public TblCostumer GetDataById(int id)
		{
			TblCostumer result = new TblCostumer();
			result = db.TblCostumers.Where(a => a.CostumerId == id).FirstOrDefault()!;
			return result;
		}

		[HttpPost("Save")]
		public VMResponse Save(TblCostumer data)
		{
			data.CreateDate = DateTime.Now;
			data.IsDelete = false;

			try
			{
				db.Add(data); db.SaveChanges();

				respon.Message = "Data succses saved";
			}
			catch (Exception e)
			{
				respon.Success = false;
				respon.Message = "Failed Saved" + e.Message;
			}
			return respon;
		}

		[HttpPut("Edit")]
		public VMResponse Edit(TblCostumer data)
		{
			TblCostumer dta = db.TblCostumers.Where(a => a.CostumerId == data.CostumerId).FirstOrDefault()!;

			if (dta != null)
			{

				dta.CostumerName = data.CostumerName;
				dta.CostumerAddress = data.CostumerAddress;
				dta.Gender = data.Gender;
				dta.TglLahir = data.TglLahir;
				dta.IsDelete = false;
				dta.CreateDate = DateTime.Now;

				try
				{
					db.Update(dta); db.SaveChanges();

					respon.Message = "Data Succes Update";
				}
				catch (Exception e)
				{
					respon.Success = false;
					respon.Message = "Data Failed" + e.Message;
				}

			}
			else
			{
				respon.Success = false;
				respon.Message = "Data Not Found";
			}
			return respon;
		}

        [HttpDelete("Delete/{id}")]
        public VMResponse Delete(int id)
        {
            TblCostumer dta = db.TblCostumers.Where(a => a.CostumerId == id).FirstOrDefault()!;

            if (dta != null)
            {
                dta.IsDelete = true;
                dta.CreateDate = DateTime.Now;

                try
                {
                    db.Remove(dta);
                    db.SaveChanges();

                    respon.Message = $"Data {dta.CostumerName} success deleted";
                }
                catch (Exception e)
                {
                    respon.Success = false;
                    respon.Message = "Delete Failed" + e.Message;
                }

            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not Found";
            }
            return respon;
        }
    }
}
