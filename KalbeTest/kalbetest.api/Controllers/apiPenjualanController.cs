﻿using kalbetest.datamodels;
using kalbetest.viewmodels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace kalbetest.api.Controllers
{
	[Route("[controller]")]
	[ApiController]
	public class apiPenjualanController : ControllerBase
	{
		private readonly kalbe_dbContext db;
		private VMResponse respon = new VMResponse();

		public apiPenjualanController(kalbe_dbContext _db)
		{
			this.db = _db;
		}

		[HttpGet("GetAllData")]
		public List<VMTblPenjualan> GetAllData()
		{
			List<VMTblPenjualan> data = (from pe in db.TblPenjualans
									  join c in db.TblCostumers on pe.CostumerId equals c.CostumerId
									  join pr in db.TblProduks on pe.ProdukId equals pr.ProdukId
									  where pe.IsDelete == false
									  select new VMTblPenjualan
									  {
										  SalesOrderId = pe.SalesOrderId,
										  DateOrder = pe.DateOrder,
										  Quantity = pe.Quantity,
										  

										  CostumerId = c.CostumerId,
										  CostumerName = c.CostumerName,

										  ProdukId = pr.ProdukId,
										  ProductName = pr.ProductName

									  }).ToList();

			return data;
		}

		[HttpGet("GetDataById/{id}")]
		public VMTblPenjualan GetDataById(int id)
		{
			VMTblPenjualan data = (from pe in db.TblPenjualans
								join c in db.TblCostumers on pe.CostumerId equals c.CostumerId
								join pr in db.TblProduks on pe.ProdukId equals pr.ProdukId
								where pe.IsDelete == false && pe.SalesOrderId == id
								select new VMTblPenjualan
								{
									SalesOrderId = pe.SalesOrderId,
									DateOrder = pe.DateOrder,
									Quantity = pe.Quantity,

									CostumerId = pe.CostumerId,
									CostumerName = c.CostumerName,

									ProdukId = pe.ProdukId,
									ProductName = pr.ProductName

								}).FirstOrDefault()!;

			return data;
		}
		[HttpPut("Edit")]
		public VMResponse Edit(TblPenjualan data)
		{
			TblPenjualan dt = db.TblPenjualans.Where(a => a.SalesOrderId == data.SalesOrderId).FirstOrDefault()!;
			if (dt != null)
			{
				int qty_old = dt.Quantity;
				dt.DateOrder = data.DateOrder;
				dt.Quantity = data.Quantity;
				dt.CostumerId = data.CostumerId;
				dt.ProdukId = data.ProdukId;
				try
				{
					db.Update(dt);

					TblProduk produk = db.TblProduks.Where(a => a.ProdukId == dt.ProdukId).FirstOrDefault()!;

					if (data.Quantity > qty_old)
					{
						produk.Quantity = produk.Quantity - (data.Quantity - qty_old);
					}
					else if (data.Quantity < qty_old)
					{
						produk.Quantity = produk.Quantity - (data.Quantity + qty_old);
					}
					db.Update(produk);
					db.SaveChanges();
					respon.Message = "success";
				}
				catch (Exception e)
				{
					respon.Success = false;
					respon.Message = "update failed :" + e.Message;
				}
			}
			else
			{
				respon.Success = false;
				respon.Message = "data not found";

			}
			return respon;
		}

		[HttpPost("Save")]
		public VMResponse Save(TblPenjualan data)
		{
			data.DateOrder = DateTime.Now;
			
			data.IsDelete = false;

			try
			{
				db.Add(data);
				//db.SaveChanges();

				TblProduk produk = db.TblProduks.Where(a => a.ProdukId == data.ProdukId).FirstOrDefault()!;
				produk.Quantity = produk.Quantity - data.Quantity;

				db.Update(produk);
				db.SaveChanges();

				respon.Message = "Data success saved";
			}
			catch (Exception e)
			{
				respon.Success = false;
				respon.Message = "Failed saved : " + e.Message;
			}
			return respon;
		}
        [HttpDelete("Delete/{id}")]
        public VMResponse Delete(int id)
        {
            TblPenjualan dt = db.TblPenjualans.Where(a => a.SalesOrderId == id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.IsDelete = true;
                dt.UpdateDate = DateTime.Now;

                try
                {
                    db.Remove(dt);

                    TblProduk produk = db.TblProduks.Where(a => a.ProdukId == dt.ProdukId).FirstOrDefault()!;
                    produk.Quantity = produk.Quantity + dt.Quantity;

                    db.Remove(produk);

                    db.SaveChanges();
                    respon.Message = $"Data success delete";
                }
                catch (Exception e)
                {
                    respon.Success = false;
                    respon.Message = "Delete failed : " + e.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not found";
            }

            return respon;
        }
    }
}
