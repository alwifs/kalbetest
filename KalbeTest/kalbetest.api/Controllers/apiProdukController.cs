﻿using kalbetest.viewmodels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using kalbetest.datamodels;

namespace kalbetest.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiProdukController : ControllerBase
    {
        private readonly kalbe_dbContext db;
        private VMResponse respon = new VMResponse();
    

        public apiProdukController(kalbe_dbContext _db)
        {
            this.db = _db;
        }

        [HttpGet("GetAllData")]
        public List<TblProduk> GetAllData()
        {
            List<TblProduk> data = db.TblProduks.Where(a => a.IsDelete == false).ToList();
            return data;

        }
        [HttpGet("GetDataById/{id}")]
        public TblProduk GetDataById(int id)
        {
            TblProduk result = new TblProduk();
            result = db.TblProduks.Where(a => a.ProdukId == id).FirstOrDefault()!;
            return result;
        }

        [HttpPost("Save")]
        public VMResponse Save(TblProduk data)
        {
			data.ProductCode = GenerateCode();
			data.CreateDate = DateTime.Now;
            data.IsDelete = false;

            try
            {
                db.Add(data); db.SaveChanges();

                respon.Message = "Data succses saved";
            }
            catch (Exception e)
            {
                respon.Success = false;
                respon.Message = "Failed Saved" + e.Message;
            }
            return respon;
        }

		[HttpGet("GenerateCode")]
		public string GenerateCode()
		{
			string code = "P";
			string digit = "";

			TblProduk data = db.TblProduks.OrderByDescending(a => a.ProductCode).FirstOrDefault()!;

			if (data != null)
			{
				string codeLast = data.ProductCode;
				string[] codeSplit = codeLast.Split("P");
				int intLast = int.Parse(codeSplit[1]) + 1;
				digit = intLast.ToString("0000");
			}
			else
			{
				digit = "0001";
			}
			return code + digit;
		}



		[HttpPut("Edit")]
        public VMResponse Edit(TblProduk data)
        {
            TblProduk dta = db.TblProduks.Where(a => a.ProdukId == data.ProdukId).FirstOrDefault()!;

            if (dta != null)
            {

                dta.ProductName = data.ProductName;
                dta.ProductCode = data.ProductCode;
                dta.Quantity = data.Quantity;
                dta.Price = data.Price;
                dta.IsDelete = false;
                dta.CreateDate = DateTime.Now;

                try
                {
                    db.Update(dta); db.SaveChanges();

                    respon.Message = "Data Succes Update";
                }
                catch (Exception e)
                {
                    respon.Success = false;
                    respon.Message = "Data Failed" + e.Message;
                }

            }
            else
            {
                respon.Success = false;
                respon.Message = "Data Not Found";
            }
            return respon;
        }

        [HttpDelete("Delete/{id}")]
        public VMResponse Delete(int id)
        {
            TblProduk dta = db.TblProduks.Where(a => a.ProdukId == id).FirstOrDefault()!;

            if (dta != null)
            {
                dta.IsDelete = true;
                dta.CreateDate = DateTime.Now;

                try
                {
                    db.Remove(dta);
                    db.SaveChanges();

                    respon.Message = $"Data {dta.ProductName} success deleted";
                }
                catch (Exception e)
                {
                    respon.Success = false;
                    respon.Message = "Delete Failed" + e.Message;
                }

            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not Found";
            }
            return respon;
        }
    }
}
