﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace kalbetest.datamodels
{
    [Table("TblCostumer")]
    public partial class TblCostumer
    {
        [Key]
        [Column("Costumer_Id")]
        public int CostumerId { get; set; }
        [Column("Costumer_Name")]
        [StringLength(50)]
        [Unicode(false)]
        public string CostumerName { get; set; } = null!;
        [Column("Costumer_Address")]
        [StringLength(50)]
        [Unicode(false)]
        public string CostumerAddress { get; set; } = null!;
        public bool Gender { get; set; }
        [Column("Tgl_Lahir", TypeName = "datetime")]
        public DateTime? TglLahir { get; set; }
        public bool? IsDelete { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreateDate { get; set; }
    }
}
