﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace kalbetest.datamodels
{
    [Table("TblPenjualan")]
    public partial class TblPenjualan
    {
        [Key]
        [Column("SalesOrderID")]
        public int SalesOrderId { get; set; }
        [Column("Costumer_Id")]
        public int CostumerId { get; set; }
        [Column("Produk_Id")]
        public int ProdukId { get; set; }
        public int Quantity { get; set; }
        public bool? IsDelete { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? DateOrder { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? UpdateDate { get; set; }
    }
}
