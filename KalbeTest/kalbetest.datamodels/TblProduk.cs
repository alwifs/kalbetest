﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace kalbetest.datamodels
{
    [Table("TblProduk")]
    public partial class TblProduk
    {
        [Key]
        [Column("Produk_Id")]
        public int ProdukId { get; set; }
        [Column("Product_Code")]
        [StringLength(50)]
        [Unicode(false)]
        public string ProductCode { get; set; } = null!;
        [Column("Product_Name")]
        [StringLength(50)]
        [Unicode(false)]
        public string ProductName { get; set; } = null!;
        public int Quantity { get; set; }
        [Column(TypeName = "decimal(18, 0)")]
        public decimal? Price { get; set; }
        public bool? IsDelete { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime CreateDate { get; set; }
    }
}
