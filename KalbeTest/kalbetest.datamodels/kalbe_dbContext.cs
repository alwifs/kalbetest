﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace kalbetest.datamodels
{
    public partial class kalbe_dbContext : DbContext
    {
        public kalbe_dbContext()
        {
        }

        public kalbe_dbContext(DbContextOptions<kalbe_dbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TblCostumer> TblCostumers { get; set; } = null!;
        public virtual DbSet<TblPenjualan> TblPenjualans { get; set; } = null!;
        public virtual DbSet<TblProduk> TblProduks { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=.;Initial Catalog=kalbe_db;Trusted_Connection=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TblCostumer>(entity =>
            {
                entity.HasKey(e => e.CostumerId)
                    .HasName("PK__TblCostu__0E5B74FCEF847B11");
            });

            modelBuilder.Entity<TblPenjualan>(entity =>
            {
                entity.HasKey(e => e.SalesOrderId)
                    .HasName("PK__TblPenju__B14003C23DA3CEE7");
            });

            modelBuilder.Entity<TblProduk>(entity =>
            {
                entity.HasKey(e => e.ProdukId)
                    .HasName("PK__TblProdu__F53B6525D1FBF7B7");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
