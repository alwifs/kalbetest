﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kalbetest.viewmodels
{
	public class PaginationList<T> : List<T>
	{
		public int PageIndex { get; set; }
		public int TotalPages { get; set; }
		public int TotalData { get; set; }
		public PaginationList(List<T> item, int count, int pageIndex, int pageSize)
		{
			PageIndex = pageIndex;
			TotalPages = (int)Math.Ceiling(count / (double)pageSize);
			TotalData = count;

			this.AddRange(item);
		}


		public bool HasPreviousPage => PageIndex > 1;
		public bool HasNextPage => PageIndex < TotalPages;
		public static PaginationList<T> CreateAsync(List<T> source, int pageIndex, int pageSize)
		{
			var count = source.Count();
			var item = source.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
			return new PaginationList<T>(item, count, pageIndex, pageSize);
		}

	}
}
