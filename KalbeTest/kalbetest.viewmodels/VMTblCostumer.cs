﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kalbetest.viewmodels
{
	public class VMTblCostumer
	{
		public int CostumerId { get; set; }
		public string CostumerName { get; set; } = null!;
		public string CostumerAddress { get; set; } = null!;
		public bool Gender { get; set; }
		public DateTime? TglLahir { get; set; }
		public DateTime? CreateDate { get; set; }
	}
}
