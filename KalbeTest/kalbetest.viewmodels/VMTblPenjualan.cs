﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kalbetest.viewmodels
{
	public class VMTblPenjualan
	{
        public int SalesOrderId { get; set; }
        public int CostumerId { get; set; }
        public string CostumerName { get; set; } = null!;
        public int ProdukId { get; set; }
        public string ProductName { get; set; } = null!;
        public int? Quantity { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? DateOrder { get; set; }
        public DateTime? UpdateDate { get; set; }
    }
}
