﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kalbetest.viewmodels
{
	public class VMTblProduct
	{
		public int ProdukId { get; set; }
		public string ProductCode { get; set; } = null!;
		public string ProductName { get; set; } = null!;
		public int Quantity { get; set; }
		public decimal? Price { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? CreateDate { get; set; }
	}
}
