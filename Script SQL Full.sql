USE [master]
GO
/****** Object:  Database [kalbe_db]    Script Date: 14/04/2023 16:44:48 ******/
CREATE DATABASE [kalbe_db]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'kalbe_db', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.MSSQLSERVER\MSSQL\DATA\kalbe_db.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'kalbe_db_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.MSSQLSERVER\MSSQL\DATA\kalbe_db_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT, LEDGER = OFF
GO
ALTER DATABASE [kalbe_db] SET COMPATIBILITY_LEVEL = 160
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [kalbe_db].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [kalbe_db] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [kalbe_db] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [kalbe_db] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [kalbe_db] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [kalbe_db] SET ARITHABORT OFF 
GO
ALTER DATABASE [kalbe_db] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [kalbe_db] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [kalbe_db] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [kalbe_db] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [kalbe_db] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [kalbe_db] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [kalbe_db] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [kalbe_db] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [kalbe_db] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [kalbe_db] SET  ENABLE_BROKER 
GO
ALTER DATABASE [kalbe_db] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [kalbe_db] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [kalbe_db] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [kalbe_db] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [kalbe_db] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [kalbe_db] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [kalbe_db] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [kalbe_db] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [kalbe_db] SET  MULTI_USER 
GO
ALTER DATABASE [kalbe_db] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [kalbe_db] SET DB_CHAINING OFF 
GO
ALTER DATABASE [kalbe_db] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [kalbe_db] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [kalbe_db] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [kalbe_db] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [kalbe_db] SET QUERY_STORE = ON
GO
ALTER DATABASE [kalbe_db] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 1000, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO, MAX_PLANS_PER_QUERY = 200, WAIT_STATS_CAPTURE_MODE = ON)
GO
USE [kalbe_db]
GO
/****** Object:  Table [dbo].[TblCostumer]    Script Date: 14/04/2023 16:44:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblCostumer](
	[Costumer_Id] [int] IDENTITY(1,1) NOT NULL,
	[Costumer_Name] [varchar](50) NOT NULL,
	[Costumer_Address] [varchar](50) NOT NULL,
	[Gender] [bit] NOT NULL,
	[Tgl_Lahir] [datetime] NULL,
	[IsDelete] [bit] NULL,
	[CreateDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Costumer_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblPenjualan]    Script Date: 14/04/2023 16:44:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblPenjualan](
	[SalesOrderID] [int] IDENTITY(1,1) NOT NULL,
	[Costumer_Id] [int] NOT NULL,
	[Produk_Id] [int] NOT NULL,
	[Quantity] [int] NULL,
	[IsDelete] [bit] NULL,
	[DateOrder] [datetime] NULL,
	[UpdateDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[SalesOrderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblProduk]    Script Date: 14/04/2023 16:44:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblProduk](
	[Produk_Id] [int] IDENTITY(1,1) NOT NULL,
	[Product_Code] [varchar](50) NOT NULL,
	[Product_Name] [varchar](50) NOT NULL,
	[Quantity] [int] NOT NULL,
	[Price] [decimal](18, 0) NULL,
	[IsDelete] [bit] NULL,
	[CreateDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Produk_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[TblCostumer] ON 

INSERT [dbo].[TblCostumer] ([Costumer_Id], [Costumer_Name], [Costumer_Address], [Gender], [Tgl_Lahir], [IsDelete], [CreateDate]) VALUES (5, N'Alwi Fadli', N'Jl. Hidup Baru Jaksel', 0, CAST(N'1996-03-31T00:00:00.000' AS DateTime), 0, CAST(N'2023-04-14T16:13:07.567' AS DateTime))
INSERT [dbo].[TblCostumer] ([Costumer_Id], [Costumer_Name], [Costumer_Address], [Gender], [Tgl_Lahir], [IsDelete], [CreateDate]) VALUES (6, N'Hamidah', N'Jl. Bangko Pusako', 1, CAST(N'1997-08-17T00:00:00.000' AS DateTime), 0, CAST(N'2023-04-14T16:13:45.823' AS DateTime))
SET IDENTITY_INSERT [dbo].[TblCostumer] OFF
GO
SET IDENTITY_INSERT [dbo].[TblPenjualan] ON 

INSERT [dbo].[TblPenjualan] ([SalesOrderID], [Costumer_Id], [Produk_Id], [Quantity], [IsDelete], [DateOrder], [UpdateDate]) VALUES (1, 3, 7, 10, 1, CAST(N'2023-04-14T00:00:00.000' AS DateTime), CAST(N'2023-04-14T16:05:18.817' AS DateTime))
INSERT [dbo].[TblPenjualan] ([SalesOrderID], [Costumer_Id], [Produk_Id], [Quantity], [IsDelete], [DateOrder], [UpdateDate]) VALUES (3, 3, 9, 5, 1, CAST(N'2023-04-14T00:00:00.000' AS DateTime), CAST(N'2023-04-14T15:34:06.000' AS DateTime))
INSERT [dbo].[TblPenjualan] ([SalesOrderID], [Costumer_Id], [Produk_Id], [Quantity], [IsDelete], [DateOrder], [UpdateDate]) VALUES (4, 2, 7, 10, 1, CAST(N'2023-04-14T15:25:39.290' AS DateTime), CAST(N'2023-04-14T16:05:15.087' AS DateTime))
INSERT [dbo].[TblPenjualan] ([SalesOrderID], [Costumer_Id], [Produk_Id], [Quantity], [IsDelete], [DateOrder], [UpdateDate]) VALUES (5, 5, 14, 10, 0, CAST(N'2023-04-14T16:14:12.713' AS DateTime), NULL)
INSERT [dbo].[TblPenjualan] ([SalesOrderID], [Costumer_Id], [Produk_Id], [Quantity], [IsDelete], [DateOrder], [UpdateDate]) VALUES (6, 6, 16, 20, 0, CAST(N'2023-04-14T16:14:29.730' AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[TblPenjualan] OFF
GO
SET IDENTITY_INSERT [dbo].[TblProduk] ON 

INSERT [dbo].[TblProduk] ([Produk_Id], [Product_Code], [Product_Name], [Quantity], [Price], [IsDelete], [CreateDate]) VALUES (15, N'P0002', N'Milna Baby', 50, CAST(20000 AS Decimal(18, 0)), 0, CAST(N'2023-04-14T16:11:58.000' AS DateTime))
INSERT [dbo].[TblProduk] ([Produk_Id], [Product_Code], [Product_Name], [Quantity], [Price], [IsDelete], [CreateDate]) VALUES (17, N'P0004', N'Morinaga', 100, CAST(35000 AS Decimal(18, 0)), 0, CAST(N'2023-04-14T16:12:37.433' AS DateTime))
INSERT [dbo].[TblProduk] ([Produk_Id], [Product_Code], [Product_Name], [Quantity], [Price], [IsDelete], [CreateDate]) VALUES (18, N'P0005', N'Prenagen', 50, CAST(10000 AS Decimal(18, 0)), 0, CAST(N'2023-04-14T16:39:23.413' AS DateTime))
SET IDENTITY_INSERT [dbo].[TblProduk] OFF
GO
USE [master]
GO
ALTER DATABASE [kalbe_db] SET  READ_WRITE 
GO
